import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
export interface Organiser {
  name: string;
}
export interface Attendees {
  name: string;
}
@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.scss']
})

export class CreateBookingComponent implements OnInit {
  minDate: Date;
  maxDate: Date;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  sel = 'Never';
  // readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  Organiser: Organiser[] = [
    {name: 'Reshmi'},
    {name: 'Akshay'}
  ];
  Attendees: Attendees[] =[
    {name:'Smriti'},
    {name:'Abinav'},
    {name:'karthik'}
  ]
  

  constructor() {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(Date.now());
    this.maxDate = new Date(currentYear + 1, 11, 31);
   }

availableLOc=[
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"},
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"},
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"},
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"},
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"},
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"},
  {"title":"coWrks","size":"800m","pax":"6-10pax","price":" 500/30mins","address":"4th Main Road, Sector 10, Sadanandanagar, Bennigana Halli"}
]
notice=[{
  notice:`The following locations are not available few days.
  If you proceed with this booking you will be added to the waitlist.
   A notification will be send when they're avaliable.`,
   address:[
     {add1:"Ving 2233,Indiranagar",date:"5th and 10th March"},
     {add1:"ving 2233, MG Road", date:"5th and 10th March"}
    ]
}]
meetcost = [
  {"location":"Coworks, indiranagar","roomsize":"3-6 members","rate":150,"priod":30,"time":60,"price":300 },
  {"location":"Coworks, indiranagar","roomsize":"3-6 members","rate":100,"priod":30,"time":60,"price":200 }
]
meettotal:any;
  ngOnInit(): void {
    this.totlemeetcost()
  }

  add(event: MatChipInputEvent,val): void {
    const input = event.input;
    const value = event.value;
    console.log(val)
    // Add our fruit
    if(val === "orga"){
      if ((value || '').trim()) {
        this.Organiser.push({name: value.trim()});
      }
  
    } else if(val=== "atte"){
      if ((value || '').trim()) {
        this.Attendees.push({name: value.trim()});
      }
  
    }
    
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(Organiser: Organiser): void {
    const index = this.Organiser.indexOf(Organiser);

    if (index >= 0) {
      this.Organiser.splice(index, 1);
    }
  }
  remove1(Attendees: Attendees): void {
    const index = this.Attendees.indexOf(Attendees);

    if (index >= 0) {
      this.Attendees.splice(index, 1);
    }
  }

totlemeetcost(){
  this.meettotal = this.meetcost.reduce((a,b) => a += b.price,0)
}

}


